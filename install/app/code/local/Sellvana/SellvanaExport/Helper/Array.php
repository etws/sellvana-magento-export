<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Helper_Array
 */
class Sellvana_SellvanaExport_Helper_Array extends Mage_Core_Helper_Abstract
{
    /**
     * @param $array
     * @param $key
     * @param null|mixed $default
     * @return null
     */
    public function get($array, $key, $default = null)
    {
        if ($key instanceof \Closure) {
            return $key($array, $default);
        }
        if (is_array($key)) {
            $lastKey = array_pop($key);
            foreach ($key as $keyPart) {
                $array =$this->get($array, $keyPart);
            }
            $key = $lastKey;
        }
        if (is_array($array) && array_key_exists($key, $array)) {
            return $array[$key];
        }
        if (($pos = strrpos($key, '.')) !== false) {
            $array =$this->get($array, substr($key, 0, $pos), $default);
            $key = substr($key, $pos + 1);
        }
        if (is_object($array)) {
            return $array->$key;
        } elseif (is_array($array)) {
            return array_key_exists($key, $array) ? $array[$key] : $default;
        } else {
            return $default;
        }
    }

    /**
     * @param $array
     * @param $key
     */
    public function remove(&$array, $key)
    {
        if (is_array($array) && (isset($array[$key]) || array_key_exists($key, $array))) {
            unset($array[$key]);
        }
    }

    /**
     * @param $array
     * @param $from
     * @param $to
     * @param null $group
     * @return array
     */
    public function map($array, $from, $to, $group = null)
    {
        $result = [];
        foreach ($array as $element) {
            $key =$this->get($element, $from);
            $value =$this->get($element, $to);
            if ($group !== null) {
                $result[$this->get($element, $group)][$key] = $value;
            } else {
                $result[$key] = $value;
            }
        }
        return $result;
    }
}