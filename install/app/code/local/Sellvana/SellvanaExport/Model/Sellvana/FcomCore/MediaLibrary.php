<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_FcomCore_MediaLibrary
 */
class Sellvana_SellvanaExport_Model_Sellvana_FcomCore_MediaLibrary
    extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    protected $_sellvanaModelName = 'FCom_Core_Model_MediaLibrary';
    protected $_magentoModelName  = 'catalog/product';
    protected $_modelGroups       = array(
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_PRODUCT
    );
    protected $_uniqueKey        = array('folder', 'subfolder', 'file_name');

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap = array(
        'id'              => 'PK',
        'folder'          => 'sellvana_folder',
        'subfolder'       => 'sellvana_subfolder',
        'file_name'       => 'sellvana_file_name',
        'file_size'       => 'sellvana_file_size',
        '_custom_data' => 'sellvana_custom_data'
    );

    protected $_duplicates;

    protected $_mageMediaUrl;
    protected $_sellvanaMediaPath = 'media/product/images';


    /**
     * @inheritdoc
     */
    public function _construct()
    {
        parent::_construct();

        /**
         * @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductConfigurable::_export,
         * @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductSimple::_export
         */
        $this->setData('processed_products', $this->_storage->getProductData('processed', 'products'));

        $this->_mageMediaUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'catalog/product';

        /** @var $dbi Varien_Db_Adapter_Interface */
        $dbi = Mage::getSingleton('core/resource')->getConnection('core_read');

        $select = $dbi->select();

        $select->from(
            array("main_table" => $dbi->getTableName('catalog_product_entity_media_gallery')),
            array('value')
        )
            ->columns('GROUP_CONCAT(value_id SEPARATOR \', \') AS ids')
            ->columns('COUNT(*) AS repeat_count')
            ->group('value')
            ->having('repeat_count > ?', 1);
        $rows = $dbi->fetchAll($select);

        /** @var Sellvana_SellvanaExport_Helper_Array $arrayHelper */
        $arrayHelper = Mage::helper('sellvana_sellvanaexport/array');

        $this->_duplicates = $arrayHelper->map($rows, 'value', 'ids');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), array(
            'validate' => array(
                'PK'                 => array('ruleVirtualAutoIncrement', 'ruleString'),
                'sellvana_folder'    => 'ruleMediaPath',
                /** @see ruleCustomData() */
                'sellvana_custom_data'      => 'ruleCustomData',
                'sellvana_file_name' => 'ruleFileName'
            ),
            'skip' => array(/*'skipProduct',*/'skipDuplicates')
        ));
    }

    /**
     * @inheritdoc
     */
    protected function _export()
    {

        $dbi = Mage::getSingleton('core/resource')->getConnection('core_read');

        $select = $dbi->select();

        // TODO: If export does not take MultiSite data into account, limit select to default store and store with id 0
        $select->from(
            array("main_table" => $dbi->getTableName('catalog_product_entity_media_gallery'))
        );

        /** @var Sellvana_SellvanaExport_Model_Sellvana_Virtual $model */
        //$model = Mage::getModel('sellvana_sellvanaexport/sellvana_virtual');
        $model = new Varien_Object();

        $counter = 0;
        $offsetSize = $this->_selectOffsetSize;
        while (true) {
            $select->limit($offsetSize, $offsetSize * $counter++);

            $rows = $dbi->fetchAll($select);

            if (count($rows) == 0 && $counter ==0) {
                return null;
            }
            if (count($rows) == 0) {
                break;
            }

            foreach ($rows as $row) {
                $model->setData($row);

                $modelData = $this->_prepareData($model);

                if ($this->skip($model)) {
                    continue;
                }

                $this->_storage->setMediaLibraryData(
                    $model->getData('value'),
                    $model->getData('PK'),
                    'productImageFile'
                );

                $this->writeToFile($modelData);
            }
        }
        return $this;
    }

    /**
     * @param Varien_Object $model
     * @return bool
     */
    public function skipProduct(Varien_Object $model)
    {
        $productId = $model->getData('entity_id');
        $processedProducts = $this->getData('processed_products');

        return (!array_key_exists($productId, $processedProducts));
    }

    /**
     * @param Varien_Object $model
     * @return bool
     */
    public function skipDuplicates(Varien_Object $model)
    {
        if (array_key_exists($model->getData('value'), $this->_duplicates)
            && strpos($this->_duplicates[$model->getData('value')], $model->getData('value_id')) !== false
        ) {
            $fileName = $model->getData('value');
            $storageValueId = $this->_storage->getMediaLibraryData($fileName, 'productImageFile');

            if (null === $storageValueId) {
                $this->_storage->setMediaLibraryData($fileName, $model->getData('PK'), 'productImageFile');
            } else {
                return true;
            }
        }
        return false;
    }

    /**
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleMediaPath(Varien_Object $model, $attribute)
    {
        $model->setData($attribute, $this->_sellvanaMediaPath);
        return true;
    }

    /**
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleFileName(Varien_Object $model, $attribute)
    {
        $fileName = explode('/', $model->getData('value'));
        $fileName = array_pop($fileName);
        $model->setData($attribute, $fileName);

        return true;
    }

    /**
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleCustomData(Varien_Object $model, $attribute)
    {
        $fileName = $model->getData('value');
        $model->setData($attribute, (object)array(
            'import_download_link' => $this->_mageMediaUrl . $fileName,
            '_merge' => false
        ));

        return true;
    }
}