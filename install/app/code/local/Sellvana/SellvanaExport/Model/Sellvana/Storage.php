<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_Storage
 */
class Sellvana_SellvanaExport_Model_Sellvana_Storage
{
    protected $_data;

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        $this->_data = array(
            'products' => array(),
            'multi_sites' => array()
        );
    }

    /**
     * Set data to storage.
     *
     * @param $key
     * @param $data
     * @param $id
     * @param $type
     */
    public function setData($key, $data, $id, $type)
    {
        $this->_data[$type][$id][$key] = $data;
    }

    /**
     * Get data from storage.
     *
     * @param $key
     * @param $id
     * @param $type
     * @return null
     */
    public function getData($key, $id, $type)
    {
        if (array_key_exists($type, $this->_data)
            && array_key_exists($id, $this->_data[$type])
            && array_key_exists($key, $this->_data[$type][$id])
        ) {
            return $this->_data[$type][$id][$key];
        }

        return null;
    }

    /**
     * Set data to storage.
     *
     * @param string $key attribute
     * @param mixed $data
     * @param int $productId
     */
    public function setProductData($key, $data, $productId)
    {
        $this->setData($key, $data, $productId, 'products');
    }

    /**
     * Get data from storage.
     *
     * @param string $key attribute
     * @param int $productId
     * @return mixed
     */
    public function getProductData($key, $productId)
    {
        return $this->getData($key, $productId, 'products');
    }

    /**
     * Set sellvana site_id by scope_id, scopes - website|store|storeView
     *
     * @param int $magentoId id of website|store|storeView
     * @param int $siteId id in sellvata
     * @param string $type website|store|storeView
     */
    public function setMultisiteData($magentoId, $siteId, $type)
    {
        $this->setData($magentoId, $siteId, $type, 'multi_sites');
    }

    /**
     * Get sellvana site_id by scope_id, scopes - website|store|storeView
     * /**
     * @param int $magentoId id of website|store|storeView
     * @param string $type website|store|storeView
     * @return null|int
     */
    public function getMultisiteData($magentoId, $type)
    {
        return $this->getData($magentoId, $type, 'multi_sites');
    }

    /**
     * @param $key
     * @param $data
     * @param $type
     */
    public function setMediaLibraryData($key, $data, $type)
    {
        $this->setData($key, $data, $type, 'media_library');
    }

    /**
     * @param $key
     * @param $type
     * @return null
     */
    public function getMediaLibraryData($key, $type)
    {
        return $this->getData($key, $type, 'media_library');
    }

    /**
     * @param $siteId
     * @param $path
     * @param $value
     */
    public function setExternalConfig($siteId, $path, $value)
    {
        $this->setData($siteId, $value, $path, 'external_config');
    }

    /**
     * @return mixed
     */
    public function getExternalConfig()
    {
        return $this->_data['external_config'];
    }

    /**
     * @param $key
     * @param $data
     * @param $type
     */
    public function setCatalogFieldsData($key, $data, $type)
    {
        $this->setData($key, $data, $type, 'catalog_fields');
    }

    /**
     * @param $key
     * @param $type
     * @return null
     */
    public function getCatalogFieldsData($key, $type)
    {
        return $this->getData($key, $type, 'catalog_fields');
    }
}