<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_CatalogCategory
 */
class Sellvana_SellvanaExport_Model_Sellvana_Catalog_Category extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    protected $_sellvanaModelName = 'Sellvana_Catalog_Model_Category';
    protected $_magentoModelName = 'catalog/category';
    protected $_modelGroups = array(
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_CATEGORY
    );
    protected $_uniqueKey = 'url_path';

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap = array(
        'id'                    => 'PK',
        //'parent_id'             => 'parent_id',
        //'level'                 => 'level', //Sellvana recalculates it
        'sort_order'            => 'position',
        'node_name'             => 'name',
        //'full_name'             => 'full_name', //Sellvana recalculates it
        //'url_key'               => 'url_key',
        'url_path'              => 'url_path',
        //'num_children'          => 'num_children', //Sellvana recalculates it
        //'num_descendants'       => 'children_count', //Sellvana recalculates it
        //'num_products'          => NULL,
        'is_enabled'            => 'is_active',
        //'is_virtual'            => NULL, //unused at the moment in sellvana
        'is_top_menu'           => 'include_in_menu',
        //'is_featured'           => NULL, //unused at the moment in sellvana
        //'data_serialized'       => NULL,
        //'show_content'          => NULL, //ask: Possible only if CMS Pages and Blocks export is implemented
        //'content'               => NULL, //ask: Possible only if CMS Pages and Blocks export is implemented
        'show_products'         => 'display_mode',
        //'show_sub_cat'          => NULL,
        //'layout_update'         => NULL,
        'page_title'            => 'meta_title',
        'description'           => 'description',
        'meta_description'      => 'meta_description',
        'meta_keywords'         => 'meta_keywords',
        'show_sidebar'          => 'is_anchor',
        //'show_view'             => NULL, //what need: show_content or show_view
        //'view_name'             => NULL,
        //'page_parts'            => NULL,
        'image_url'             => 'thumbnail',
        //'featured_image_url'    => NULL, //unused at the moment  in sellvana
        //'nav_callout_image_url' => NULL  //unused at the moment  in sellvana
    );

    protected $_collectionArray;
    protected $_categoryUrlPaths;
    protected $_categoryFullNames;
    protected $_rootCatId;
    protected $_rootDefaultCatId;

    public function _construct()
    {
        parent::_construct();

        $this->_getCategoryUrlPath(0);
        $this->_rootDefaultCatId = $this->_getDefaultRootCategoryId();
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), array(
            'validate' => array(
                /** @see ruleRootToNull(), ruleZeroToNull() */
                'parent_id'    => array('ruleRootToNull','ruleZeroToNull'),
                /** @see ruleNotZero() */
                'position'     => 'ruleNotZero',
                /** @see ruleFullName(), ruleUnique() */
                'full_name'    => array('ruleFullName', 'ruleUnique'),
                /** @see ruleNotNull(), ruleUrlValidate(), ruleUnique() */
                'url_key'      => array('ruleNotNull', 'ruleUrlValidate', /*'ruleUnique'*/),
                /** @see ruleUrlPath(), ruleUnique() */
                'url_path'     => array('ruleUrlPath', 'ruleUnique'),
                /** @see ruleChildrenCountByParentId() */
                'num_children' => 'ruleChildrenCountByParentId',
                /** @see ruleInt(), ruleString() */
                'is_active'    => array('ruleInt','ruleString'),
                /** @see ruleDisplayMode(), ruleInt(), ruleString() */
                'display_mode' => array('ruleDisplayMode', 'ruleInt', 'ruleString'),
                /** @see ruleIsAnchor(), ruleInt(), ruleString() */
                'is_anchor'    => array('ruleIsAnchor', 'ruleInt', 'ruleString')
            ),
            /** @see skipCategory(), skipIncorrectPath() */
            'skip' => array('skipCategory', 'skipIncorrectPath')
        ));
    }

    protected function _prepareCollection(Varien_Data_Collection_Db $collection)
    {
        parent::_prepareCollection($collection);

        $collection->getSelect()->order('level')->order('entity_id');
    }


    /**
     *
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleChildrenCountByParentId(Varien_Object $model, $attribute)
    {
        $collection = $this->_collectionArray;
        if (null === $collection) {
            $collection = Mage::getModel($this->_magentoModelName)->getCollection();
            /** @var Mage_Catalog_Model_Resource_Category_Collection $collection */
            $collection = $collection->load()->toArray(array('entity_id', 'parent_id'));
            $this->_collectionArray = $collection;
        }

        $currentId = $model->getId();

        $i = 0;
        foreach ($collection as $rowId => $row) {
            if ($row['parent_id'] == $currentId) {
                $i++;
                unset($this->_collectionArray[$rowId]);
            }
        }
        $model->setData($attribute, (string)$i);

        return true;
    }

    protected function _getDefaultRootCategoryId()
    {
        /** @var Mage_Adminhtml_Model_Config_Data $config */
        $config = Mage::getSingleton('adminhtml/config_data');
        $config->setScope('default');
        $config->setScopeId(0);
        $config->setScopeCode('');

        return (string)$config->getConfigDataValue('catalog/category/root_id');
    }

    /**
     * @param $categoryId
     * @return mixed
     * @throws Mage_Core_Exception
     */
    protected function _getCategoryUrlPath($categoryId)
    {
        if (null !== $this->_categoryUrlPaths) {
            if (!array_key_exists($categoryId, $this->_categoryUrlPaths)) {
                Mage::throwException(Mage::helper('sellvana_sellvanaexport')->__('Unknown category ID.'));
            }
            return $this->_categoryUrlPaths[$categoryId];
        }

        /** @var Sellvana_SellvanaExport_Helper_Array $arrayHelper */
        $arrayHelper = Mage::helper('sellvana_sellvanaexport/array');

        /** @var Mage_Catalog_Model_Resource_Category_Collection $collection */
        $collection = Mage::getModel($this->_magentoModelName)->getCollection();
        $collection->addAttributeToSelect('url_key', 'left');
        $collection->addAttributeToSelect('name', 'left');
        $select = $collection->getSelect();
        $db = Mage::getSingleton('core/resource')->getConnection('core_read');

        $rows = $db->fetchAll($select);

        $rootCats = array();

        foreach ($rows as $id => $row) {
            if (in_array($row['parent_id'], array(0, 1)) && in_array($row['level'], array(0, 1))) {
                $rootCats[$row['entity_id']] = $row;
            }

            if (null !== $row['url_key']) {
                continue;
            }

            $rows[$id]['url_key'] = $this->slugify($row['name']);
        }

        $rows = $arrayHelper->map($rows, 'path', 'url_key', 'entity_id');

        $root = reset($rootCats);
        $this->_rootCatId = $root['entity_id'];

        $tmpRows = $rows;
        foreach ($tmpRows as $id => $row) {
            //Skip root category
            if ($id == $this->_rootCatId) {
                continue;
            }

            $path = key($row);
            $path = explode('/', $path);

            $urlPath = array();
            foreach ($path as $pathId) {
                if ($id != $pathId && in_array($pathId, array_keys($rootCats))) {
                    continue;
                }
                $tmpUrlKey = array_values($rows[$pathId]);
                $tmpUrlKey = array_pop($tmpUrlKey);
                $urlPath[] = $tmpUrlKey;
            }
            $urlPath = implode('/', $urlPath);

            $tmpRows[$id] = $urlPath;
        }
        $rows = $tmpRows;

        $this->_categoryUrlPaths = $rows;

        return !empty($rows[$categoryId]) ? $rows[$categoryId] : '';
    }

    /**
     * @param $categoryId
     * @return mixed
     * @throws Mage_Core_Exception
     */
    protected function _getCategoryFullName($categoryId)
    {
        if (null !== $this->_categoryFullNames) {
            if (!array_key_exists($categoryId, $this->_categoryFullNames)) {
                Mage::throwException(Mage::helper('sellvana_sellvanaexport')->__('Unknown category ID.'));
            }
            return $this->_categoryFullNames[$categoryId];
        }

        /** @var Sellvana_SellvanaExport_Helper_Array $arrayHelper */
        $arrayHelper = Mage::helper('sellvana_sellvanaexport/array');

        /** @var Mage_Catalog_Model_Resource_Category_Collection $collection */
        $collection = Mage::getModel($this->_magentoModelName)->getCollection();
        $collection->addAttributeToSelect('name', 'left');
        $select = $collection->getSelect();
        $db = Mage::getSingleton('core/resource')->getConnection('core_read');

        $rows = $db->fetchAll($select);

        $rows = $arrayHelper->map($rows, 'path', 'name', 'entity_id');

        $tmpRows = $rows;
        foreach ($tmpRows as $id => $row) {
            //Skip root category
            if ($id == $this->_rootCatId) {
                continue;
            }

            $path = key($row);
            $path = explode('/', $path);

            $urlPath = array();
            foreach ($path as $pathId) {
                //Skip root category
                if ($pathId == $this->_rootCatId) {
                    continue;
                }
                $tmpUrlKey = array_values($rows[$pathId]);
                $tmpUrlKey = array_pop($tmpUrlKey);
                $urlPath[] = $tmpUrlKey;
            }
            $urlPath = implode('|', $urlPath);

            $tmpRows[$id] = $urlPath;
        }
        $rows = $tmpRows;

        $this->_categoryFullNames = $rows;

        return $rows[$categoryId];
    }

    /**
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleUrlPath(Varien_Object $model, $attribute)
    {
        $id = $model->getId();
        $model->setData($attribute, $this->_getCategoryUrlPath($id));

        return true;
    }

    /**
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleFullName(Varien_Object $model, $attribute)
    {
        $id = $model->getId();
        $model->setData($attribute, $this->_getCategoryFullName($id));

        return true;
    }

    /**
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleDisplayMode(Varien_Object $model, $attribute)
    {
        switch ($model->getData($attribute)) {
            case Mage_Catalog_Model_Category::DM_MIXED:
            case Mage_Catalog_Model_Category::DM_PRODUCT:
                $model->setData($attribute, true);
                break;
            case Mage_Catalog_Model_Category::DM_PAGE:
                $model->setData($attribute, false);
                break;
            default:
                $model->setData($attribute, true);
                break;
        }
        return true;
    }

    /**
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleIsAnchor(Varien_Object $model, $attribute)
    {
        if ($model->getData('page_layout') == '1column') {
            $model->setData($attribute, false);
            return true;
        }

        return true;
    }

    /**
     * Transform parentId to Null, if it's root category.
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleRootToNull(Varien_Object $model, $attribute)
    {
        if ($model->getData($attribute) == $this->_rootCatId) {
            $model->setData($attribute, null);
        }
        return true;
    }

    /**
     * Skip root category.
     *
     * @param Varien_Object $model
     * @return bool
     */
    public function skipCategory(Varien_Object $model)
    {
        if ($model->getId() == $this->_rootCatId || $model->getId() == $this->_rootDefaultCatId) {
            return true;
        }

        return ('0' === $model->getData('parent_id') && null === $model->getData('name'));
    }

    /**
     * Skip categories with incorrect paths
     *
     * @param Varien_Object $model
     * @return bool
     */
    public function skipIncorrectPath(Varien_Object $model)
    {
        if (substr($model->getData('path'), 0, 1) === '/') {
            return true;
        }

        $return = (count(explode('/', $model->getData('path'))) != ($model->getData('level') + 1));
        return $return;
    }

    /**
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleDefaultRootCatUrlKey(Varien_Object $model, $attribute)
    {
        if ($model->getId() == $this->_rootDefaultCatId) {
            $model->setData($attribute, null);
        }
        return true;
    }
}