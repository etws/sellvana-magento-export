<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_SetField
 */
class Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_SetField
    extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    protected $_sellvanaModelName = 'Sellvana_CatalogFields_Model_SetField';
    protected $_magentoModelName  = 'eav/entity_attribute_collection';
    protected $_modelGroups       = array(
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_ATTRIBUTE
    );
    protected $_uniqueKey        = array(
        'set_id',
        'field_id'
    );

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap = array(
        'id'       => 'PK',//"2"
        'set_id'   => 'group_id',//"2"
        'field_id' => 'attribute_id' //"2"
    );


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), array(
            'validate' => array(
                'PK'       => array('ruleVirtualAutoIncrement', 'ruleString'),
                'group_id' => 'ruleString',
                'attribute_id' => 'ruleString'
            ),
            //'skip' => array()
        ));
    }

    /**
     * @inheritdoc
     */
    protected function _export()
    {
        $entityType = Mage::getModel('catalog/product')->getResource()->getTypeId();

        /** @var Mage_Eav_Model_Resource_Entity_Attribute_Collection $collection */
        $collection = Mage::getResourceModel($this->_magentoModelName);
        $collection->addSetInfo()->setEntityTypeFilter($entityType);

        if ($collection->count() <= 0) {
            return null;
        }

        /** @var Sellvana_SellvanaExport_Model_Sellvana_Virtual $attributeModel */
        //$model = Mage::getModel('sellvana_sellvanaexport/sellvana_virtual');
        $model = new Varien_Object();

        $groupIds = $this->_storage->getCatalogFieldsData('realSetIds', 'set');

        $attributeCodes = array();

        $duplicates = array();
        /** @var Mage_Eav_Model_Entity_Attribute $model */
        foreach ($collection as $attributeModel) {

            $attributeId = $attributeModel->getData('attribute_id');
            $attributeSetInfo = $attributeModel->getData('attribute_set_info');

            if ($attributeSetInfo) {
                foreach ($attributeSetInfo as $info) {
                    $groupId = $groupIds[$info['group_id']];
                    $key = $attributeId . '/' . $groupId;

                    if (!array_key_exists($key, $duplicates)) {
                        $duplicates[$key] = true;

                        $model->setData(array(
                            'group_id' => $groupId,
                            'attribute_id' => $attributeId,
                        ));

                        $attributeCodes[$attributeId] = $groupId;

                        $modelData = $this->_prepareData($model);

                        $this->writeToFile($modelData);
                    }
                }
            }
        }
        $this->_storage->setCatalogFieldsData('attribute_codes', $attributeCodes, 'setfield');

        return $this;
    }
}