<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_Customer_Customer
 */
class Sellvana_SellvanaExport_Model_Sellvana_Customer_Customer extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    protected $_sellvanaModelName        = 'Sellvana_Customer_Model_Customer';
    protected $_magentoModelName = 'customer/customer';
    protected $_modelGroups      = array(
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_CUSTOMER
    );
    protected $_uniqueKey        = 'email';

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap = array(
        'id'                     => 'PK',//1
        'email'                  => 'email',//testemail@test.com
        'firstname'              => 'firstname',//firstname
        'lastname'               => 'lastname',//lastname
        'password_hash'          => 'password_hash',
        'default_shipping_id'    => 'default_shipping',//null
        'default_billing_id'     => 'default_billing',//null
        'create_at'              => 'created_at',//2015-10-07 12:01:04
        'update_at'              => 'updated_at',//2015-10-07 12:01:04
        //'last_login'             => '',
        //'token'                  => '',//null
        //'token_at'               => '',//null
        //'payment_method'         => '',
        //'payment_details'        => '',
        'status'                 => 'is_active',//active
        //'password_session_token' => '',
        //'last_session_id'        => '',
        'customer_group'         => 'group_id'
    );

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), array(
            'validate' => array(
                'password_hash' => 'ruleSetNull',
                'is_active' => 'ruleStatus',
                'created_at' => 'ruleMysqlDate'
            )
        ));
    }

    /**
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleStatus(Varien_Object $model, $attribute)
    {
        $model->setData($attribute, 'disabled');
        if ($model->getData($attribute) == true) {
            $model->setData($attribute, 'active');
        }

        return true;
    }
}
