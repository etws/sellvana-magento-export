<?php
class Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductLink extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    protected $_sellvanaModelName = 'Sellvana_Catalog_Model_ProductLink';
    protected $_magentoModelName  = 'catalog/product_link';
    protected $_modelGroups       = array(
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_PRODUCT
    );
    protected $_uniqueKey        = array(
        'link_type',
        'product_id',
        'linked_product_id',
    );

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap = array(
        'id'                => 'PK',
        'link_type'         => 'sellvana_link_type',
        'product_id'        => 'product_id',
        'linked_product_id' => 'linked_product_id',
        'position'          => 'position',
    );

    /** @var array MagentoLinkType => SellvanaLinkType */
    protected $_typeMapping = array(
        'relation' => 'related',
        'up_sell' => 'similar',
        'cross_sell' => 'cross_sell',
    );

    public function _construct()
    {
        parent::_construct();

        $this->setData('processed_products', (array)$this->_storage->getProductData('processed', 'products'));
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        //ruleUnique is to slow for big data.
        return array_merge_recursive(parent::rules(), array(
            'validate' => array(
                'product_id' => 'ruleString',
                'linked_product_id' => 'ruleString',
                'sellvana_link_type' => 'ruleSellvanaLinkType',
                'position' => 'ruleInt'
            ),
            'skip' => array('skipNotProcessed', 'skipUnsupportedTypes')
        ));
    }

    protected function _prepareCollection(Varien_Data_Collection_Db $collection)
    {
        parent::_prepareCollection($collection);

        $typeTable = Mage::getSingleton('core/resource')->getTableName('catalog/product_link_type');
        $collection->getSelect()->join(
            array('type' => $typeTable),
            'type.link_type_id = main_table.link_type_id',
            array('code')
        );

        $atrrTable = Mage::getSingleton('core/resource')->getTableName('catalog/product_link_attribute');
        $collection->getSelect()->joinLeft(
            array('attr' => $atrrTable),
            'attr.link_type_id = main_table.link_type_id AND attr.product_link_attribute_code = "position"',
            array()
        );

        $valueTable = Mage::getSingleton('core/resource')->getTableName('catalog/product_link_attribute_int');
        $collection->getSelect()->joinLeft(
            array('position' => $valueTable),
            'position.product_link_attribute_id = attr.product_link_attribute_id AND position.link_id = main_table.link_id',
            array('position' => 'value')
        );
    }

    /**
     * Convert link type from Magento to Sellvana format
     *
     * @param Mage_Catalog_Model_Product_Link $model
     * @param $attribute
     * @return bool
     */
    public function ruleSellvanaLinkType(Mage_Catalog_Model_Product_Link $model, $attribute)
    {
        $type = $model->getCode();
        if (array_key_exists($type, $this->_typeMapping)) {
            $model->setData($attribute, $this->_typeMapping[$type]);
        }

        return true;
    }

    /**
     * Skip if one of the products were skipped earlier
     *
     * @param Mage_Catalog_Model_Product_Link $model
     * @return bool
     */
    public function skipNotProcessed(Mage_Catalog_Model_Product_Link $model)
    {
        $processed = array_keys($this->getData('processed_products'));

        return !(in_array($model->getProductId(), $processed) && in_array($model->getLinkedProductId(), $processed));
    }

    /**
     * Skip if link type is unsupported
     *
     * @param Mage_Catalog_Model_Product_Link $model
     * @return bool
     */
    public function skipUnsupportedTypes(Mage_Catalog_Model_Product_Link $model)
    {
        return !array_key_exists($model->getCode(), $this->_typeMapping);
    }
}