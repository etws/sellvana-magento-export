<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_FcomCore_ExternalConfig
 */
class Sellvana_SellvanaExport_Model_Sellvana_FcomCore_ExternalConfig
    extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    protected $_sellvanaModelName        = 'FCom_Core_Model_ExternalConfig';
    protected $_magentoModelName = '';
    protected $_uniqueKey        = array(
        'source_type',
        'path',
        'site_id'
    );

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap = array(
        'id'          => 'virtual_auto_increment',
        'source_type' => 'source_type',
        'path'        => 'path',
        'value'       => 'value',
        'site_id'     => 'site_id'
    );

    /** @var Mage_Core_Model_Website */
    protected $_currentWebsite;

    /** @var array SellvanaPath => MagentoPath */
    protected static $_pathMap = array(
        'cookie/path' => 'web/cookie/cookie_path',
        'cookie/domain' => 'web/cookie/cookie_domain',

        'modules/FCom_Frontend/default_locale' => 'general/locale/code',

        'modules/FCom_Core/allowed_countries' => 'general/country/allow',
        'modules/FCom_Core/company_name' => 'general/store_information/name',
        'modules/FCom_Core/site_title' => 'general/store_information/name',
        'modules/FCom_Core/admin_email' => 'trans_email/ident_general/email',
        'modules/FCom_Core/sales_name' => 'trans_email/ident_sales/name',
        'modules/FCom_Core/sales_email' => 'trans_email/ident_sales/email',
        'modules/FCom_Core/support_name' => 'trans_email/ident_support/name',
        'modules/FCom_Core/support_email' => 'trans_email/ident_support/email',
        'modules/FCom_Core/copyright_message' => 'design/footer/copyright',
        'modules/FCom_Core/default_country' => 'general/country/default',
        'modules/FCom_Core/default_tz' => 'general/locale/timezone',
        'modules/FCom_Core/base_locale' => 'general/locale/code',
        'modules/FCom_Core/base_currency' => 'currency/options/base',
        'modules/FCom_Core/default_currency' => 'currency/options/default',

        // Catalog
        'modules/Sellvana_Catalog/manage_inventory' => 'cataloginventory/item_options/manage_stock',
        'modules/Sellvana_Catalog/allow_backorder' => array('callback' => '_getBackorders'),
        'modules/Sellvana_Catalog/qty_notify_admin' => 'cataloginventory/item_options/notify_stock_qty',

        // Sitemap
        'modules/Sellvana_Seo/category_changefreq' => 'sitemap/category/changefreq',
        'modules/Sellvana_Seo/category_priority' => 'sitemap/category/priority',
        'modules/Sellvana_Seo/product_changefreq' => 'sitemap/product/changefreq',
        'modules/Sellvana_Seo/product_priority' => 'sitemap/product/priority',

        // Sales
        'modules/Sellvana_Sales/store_name' => 'general/store_information/name',
        'modules/Sellvana_Sales/store_email' => 'trans_email/ident_general/email',
        'modules/Sellvana_Sales/store_city' => 'shipping/origin/city',
        'modules/Sellvana_Sales/store_region' => 'shipping/origin/region_id',
        'modules/Sellvana_Sales/store_postcode' => 'shipping/origin/postcode',
        'modules/Sellvana_Sales/store_country' => 'shipping/origin/country_id',
        'modules/Sellvana_Sales/store_street1' => 'shipping/origin/street_line1',
        'modules/Sellvana_Sales/store_street2' => 'shipping/origin/street_line2',
        'modules/Sellvana_Sales/store_phone' => 'general/store_information/phone',

        // UPS
        'modules/Sellvana_ShippingUps/access_key' => array(
            'callback' => '_decrypt',
            'params' => 'carriers/ups/access_license_number'
        ),
        'modules/Sellvana_ShippingUps/shipper_number' => 'carriers/ups/shipper_number',
        'modules/Sellvana_ShippingUps/user_id' => array(
            'callback' => '_decrypt',
            'params' => 'carriers/ups/username'
        ),
        'modules/Sellvana_ShippingUps/password' => array(
            'callback' => '_decrypt',
            'params' => 'carriers/ups/password'
        ),
        'modules/Sellvana_ShippingUps/enabled' => array('callback' => '_getUpsAllowed'),

        // AuthorizeNet
        'modules/Sellvana_PaymentAuthorizeNet/dpm/active' => 'payment/authorizenet_directpost/active',
        'modules/Sellvana_PaymentAuthorizeNet/dpm/cctypes' => array(
            'callback' => '_toArray',
            'params' => array('payment/authorizenet_directpost/cctypes')
        ),
        'modules/Sellvana_PaymentAuthorizeNet/dpm/title' => 'payment/authorizenet_directpost/title',
        'modules/Sellvana_PaymentAuthorizeNet/dpm/login' => array(
            'callback' => '_decrypt',
            'params' => 'payment/authorizenet_directpost/login'
        ),
        'modules/Sellvana_PaymentAuthorizeNet/dpm/trans_key' => array(
            'callback' => '_decrypt',
            'params' => 'payment/authorizenet_directpost/trans_key'
        ),
        'modules/Sellvana_PaymentAuthorizeNet/dpm/trans_md5' => array(
            'callback' => '_decrypt',
            'params' => 'payment/authorizenet_directpost/trans_md5'
        ),
        'modules/Sellvana_PaymentAuthorizeNet/dpm/payment_action' => 'payment/authorizenet_directpost/payment_action',
        'modules/Sellvana_PaymentAuthorizeNet/dpm/order_status' => 'payment/authorizenet_directpost/order_status',
        'modules/Sellvana_PaymentAuthorizeNet/dpm/test' => 'payment/authorizenet_directpost/test',
        'modules/Sellvana_PaymentAuthorizeNet/dpm/cgi_url' => 'payment/authorizenet_directpost/cgi_url',
        'modules/Sellvana_PaymentAuthorizeNet/dpm/currency' => 'payment/authorizenet_directpost/currency',
        'modules/Sellvana_PaymentAuthorizeNet/dpm/debug' => 'payment/authorizenet_directpost/debug',
        'modules/Sellvana_PaymentAuthorizeNet/dpm/email_customer' => 'payment/authorizenet_directpost/email_customer',
        'modules/Sellvana_PaymentAuthorizeNet/dpm/merchant_email' => 'payment/authorizenet_directpost/merchant_email',
        'modules/Sellvana_PaymentAuthorizeNet/dpm/useccv' => 'payment/authorizenet_directpost/useccv',
        'modules/Sellvana_PaymentAuthorizeNet/dpm/allowspecific' => 'payment/authorizenet_directpost/allowspecific',
        'modules/Sellvana_PaymentAuthorizeNet/dpm/specificcountry' => array(
            'callback' => '_toArray',
            'params' => 'payment/authorizenet_directpost/specificcountry'
        ),
        'modules/Sellvana_PaymentAuthorizeNet/dpm/min_order_total' => 'payment/authorizenet_directpost/min_order_total',
        'modules/Sellvana_PaymentAuthorizeNet/dpm/max_order_total' => 'payment/authorizenet_directpost/max_order_total',
        'modules/Sellvana_PaymentAuthorizeNet/dpm/sort_order' => 'payment/authorizenet_directpost/sort_order',
        'modules/Sellvana_PaymentAuthorizeNet/aim/active' => 'payment/authorizenet/active',
        'modules/Sellvana_PaymentAuthorizeNet/aim/cctypes' => array(
            'callback' => '_toArray',
            'params' => 'payment/authorizenet/cctypes'
        ),
        'modules/Sellvana_PaymentAuthorizeNet/aim/title' => 'payment/authorizenet/title',
        'modules/Sellvana_PaymentAuthorizeNet/aim/login' => array(
            'callback' => '_decrypt',
            'params' => 'payment/authorizenet/login'
        ),
        'modules/Sellvana_PaymentAuthorizeNet/aim/trans_key' => array(
            'callback' => '_decrypt',
            'params' => 'payment/authorizenet/trans_key'
        ),
        'modules/Sellvana_PaymentAuthorizeNet/aim/trans_md5' => array(
            'callback' => '_decrypt',
            'params' => 'payment/authorizenet/trans_md5'
        ),
        'modules/Sellvana_PaymentAuthorizeNet/aim/payment_action' => 'payment/authorizenet/payment_action',
        'modules/Sellvana_PaymentAuthorizeNet/aim/order_status' => 'payment/authorizenet/order_status',
        'modules/Sellvana_PaymentAuthorizeNet/aim/test' => 'payment/authorizenet/test',
        'modules/Sellvana_PaymentAuthorizeNet/aim/cgi_url' => 'payment/authorizenet/cgi_url',
        'modules/Sellvana_PaymentAuthorizeNet/aim/currency' => 'payment/authorizenet/currency',
        'modules/Sellvana_PaymentAuthorizeNet/aim/debug' => 'payment/authorizenet/debug',
        'modules/Sellvana_PaymentAuthorizeNet/aim/email_customer' => 'payment/authorizenet/email_customer',
        'modules/Sellvana_PaymentAuthorizeNet/aim/merchant_email' => 'payment/authorizenet/merchant_email',
        'modules/Sellvana_PaymentAuthorizeNet/aim/useccv' => 'payment/authorizenet/useccv',
        'modules/Sellvana_PaymentAuthorizeNet/aim/allowspecific' => 'payment/authorizenet/allowspecific',
        'modules/Sellvana_PaymentAuthorizeNet/aim/specificcountry' => array(
            'callback' => '_toArray',
            'params' => 'payment/authorizenet/specificcountry'
        ),
        'modules/Sellvana_PaymentAuthorizeNet/aim/min_order_total' => 'payment/authorizenet/min_order_total',
        'modules/Sellvana_PaymentAuthorizeNet/aim/max_order_total' => 'payment/authorizenet/max_order_total',
        'modules/Sellvana_PaymentAuthorizeNet/aim/sort_order' => 'payment/authorizenet/sort_order',

        // PayPal
        'modules/Sellvana_PaymentPaypal' => array('callback' => '_getPaypalConfig', 'multiple' => true),
    );

    /**
     * [webSiteId][Path] = value
     */
    protected $_exportData = array();

    /**
     * Prepare data
     */
    protected function _prepareWebsiteConfigs()
    {
        $websites = Mage::app()->getWebsites();
        $websites[0] = null;
        ksort($websites);

        foreach ($websites as $websiteId => $website) {
            $this->_currentWebsite = $website;
            foreach (self::$_pathMap as $sellvanaPath => $magentoPath) {
                if (is_array($magentoPath) && array_key_exists('callback', $magentoPath)) {
                    $params = array();
                    if (array_key_exists('params', $magentoPath)) {
                        $params = $magentoPath['params'];
                    }
                    if (!is_array($params)) {
                        $params = array($params);
                    }
                    $data = call_user_func_array(array($this, $magentoPath['callback']), $params);
                } else {
                    $data = (string)$this->_getConfig($magentoPath);
                }

                if (is_array($magentoPath) && array_key_exists('multiple', $magentoPath) && $magentoPath['multiple']) {
                    foreach ($data as $subPath => $value) {
                        $this->_exportData[$websiteId][$sellvanaPath . '/' . $subPath] = $value;
                    }
                } else {
                    $this->_exportData[$websiteId][$sellvanaPath] = $data;
                }
            }
        }
    }

    protected function _getConfig($path)
    {
        return $this->_currentWebsite === null ? Mage::getStoreConfig($path, 0) : $this->_currentWebsite->getConfig($path);
    }

    protected function _decrypt($path)
    {
        $value = $this->_getConfig($path);
        if ($this->_currentWebsite === null) {
            return $value;
        }

        /** @var Mage_Core_Helper_Data $helper */
        $helper = Mage::helper('core');
        return $helper->decrypt($value);
    }

    /**
     * @inheritdoc
     */
    protected function _export()
    {
        $this->_prepareWebsiteConfigs();

        $autoIncrement = 1;
        foreach ($this->_exportData as $websiteId => $websiteData) {
            foreach ($websiteData as $path => $value) {
                if ($websiteId > 0) {
                    if (!is_array($value) && $value == $this->_exportData[0][$path]) {
                        continue;
                    }

                    if (is_array($value) && count(array_diff($value, $this->_exportData[0][$path])) == 0) {
                        continue;
                    }
                }
                $data = array(
                    'id' => (string)$autoIncrement++,
                    'source_type' => 'magento',
                    'path' => $path,
                    'value' => $value,
                    'site_id' => $websiteId > 0 ? $this->_storage->getMultisiteData($websiteId, 'website') : null
                );

                $data = array_values($data);

                $this->writeToFile(json_encode($data));
            }
        }

        return $this;
    }

    protected function _getBackorders()
    {
        $value = $this->_getConfig('cataloginventory/item_options/backorders');
        return $value ? 'ALLOW_QUANTITY_BELOW' : 'NOT_BACK_ORDERS';
    }

    protected function _getUpsAllowed()
    {
        $type = $this->_getConfig('carriers/ups/type');
        return $type == 'UPS_XML' ? $this->_getConfig('carriers/ups/active') : 0;
    }

    protected function _toArray($path)
    {
        return explode(',', $this->_getConfig($path));
    }

    protected function _getPaypalConfig()
    {
        if (!$this->_getConfig('payment/paypal_express/active')) {
            return array();
        }

        $mode = $this->_getConfig('paypal/wpp/sandbox_flag') ? 'sandbox' : 'production';

        return array(
            $mode . '/mode' => 'on',
            $mode . '/username' => $this->_decrypt('paypal/wpp/api_username'),
            $mode . '/password' => $this->_decrypt('paypal/wpp/api_password'),
            $mode . '/signature' => $this->_decrypt('paypal/wpp/api_signature'),
            $mode . '/payment_action' => $this->_getConfig('payment/paypal_express/payment_action'),
        );
    }
}