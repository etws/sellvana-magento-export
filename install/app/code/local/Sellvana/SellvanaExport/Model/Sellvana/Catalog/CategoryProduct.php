<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_Catalog_CategoryProduct
 */
class Sellvana_SellvanaExport_Model_Sellvana_Catalog_CategoryProduct
    extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    protected $_sellvanaModelName        = 'Sellvana_Catalog_Model_CategoryProduct';
    protected $_magentoModelName = '';
    protected $_modelGroups      = array(
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_CATEGORY,
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_PRODUCT,
    );
    protected $_uniqueKey        = array(
        'product_id',
        'category_id'
    );

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap = array(
        'id'          => 'PK',
        'product_id'  => 'product_id',
        'category_id' => 'category_id',
        'sort_order'  => 'position'
    );

    /**
     * @inheritdoc
     */
    public function _construct()
    {
        parent::_construct();

        /**
         * @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductConfigurable::_export,
         * @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductSimple::_export
         */
        $this->setData('processed_products', $this->_storage->getProductData('processed', 'products'));
    }

    public function rules()
    {
        return array_merge_recursive(parent::rules(), array(
            'validate' => array(
                'PK' => array('ruleVirtualAutoIncrement')
            ),
            /** @see skipProduct() */
            'skip' => 'skipProduct'
        ));
    }

    /**
     * @inheritdoc
     */
    protected function _prepareCollection(Varien_Data_Collection_Db $collection)
    {
        parent::_prepareCollection($collection);

        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection->addAttributeToFilter('type_id', array('in' => array('simple', 'configurable')));
    }

    /**
     * @inheritdoc
     */
    protected function _export()
    {
        /** @var $resource Mage_Core_Model_Resource */
        $resource = Mage::getSingleton('core/resource');
        /** @var $db Varien_Db_Adapter_Interface */
        $dbi = Mage::getSingleton('core/resource')->getConnection('core_read');

        $select = $dbi->select();

        $select->from(array("main_table" => $resource->getTableName('catalog/category_product')));

        /** @var Sellvana_SellvanaExport_Model_Sellvana_Virtual $model */
        //$model = Mage::getModel('sellvana_sellvanaexport/sellvana_virtual');
        $model = new Varien_Object();

        $counter = 0;
        $offsetSize = $this->_selectOffsetSize;
        while (true) {
            $select->limit($offsetSize, $offsetSize * $counter++);

            $rows = $dbi->fetchAll($select);

            if (count($rows) == 0 && $counter ==0) {
                return null;
            }
            if (count($rows) == 0) {
                break;
            }

            foreach ($rows as $row) {
                $model->setData($row);

                if ($this->skip($model)) {
                    continue;
                }

                $modelData = $this->_prepareData($model);

                $this->writeToFile($modelData);
            }
        }

        return $this;
    }

    /**
     * Check if is necessary to skip product
     *
     * @param Varien_Object|Mage_Catalog_Model_Product $model
     * @return bool
     */
    public function skipProduct(Varien_Object $model)
    {
        $processedProducts = $this->getData('processed_products');
        $productId = $model->getdata('product_id');

        return (!array_key_exists($productId, $processedProducts));
    }
}