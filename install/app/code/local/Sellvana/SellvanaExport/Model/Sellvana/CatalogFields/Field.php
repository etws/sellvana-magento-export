<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_Field
 */
class Sellvana_SellvanaExport_Model_Sellvana_CatalogFields_Field extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    protected $_sellvanaModelName = 'Sellvana_CatalogFields_Model_Field';
    protected $_magentoModelName  = 'eav/config';
    protected $_modelGroups      = array(
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_ATTRIBUTE
    );
    protected $_uniqueKey         = 'field_code';

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap = array(
        'id'               => 'PK',
        'field_type'       => 'entity_type_id',
        'field_code'       => 'attribute_code',
        'field_name'       => 'frontend_label',
        'table_field_type' => 'backend_type',
        'admin_input_type' => 'frontend_input',
        'frontend_label'   => 'frontend_label',
        'frontend_show'    => 'is_visible_on_front',
        //'config_json'      => '',
        'sort_order'       => 'position',
        'facet_select'     => 'is_filterable',
        'system'           => 'is_user_defined',
        'multilanguage'    => 'sellvana_multilanguage',
        //'validation'       => '',
        'required'         => 'is_required',
        //'data_serialized'  => '',
        //'create_at'        => '',
        //'update_at'        => ''
    );

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), array(
            'validate' => array(
                'entity_type_id'         => 'ruleType',
                'backend_type'           => 'ruleTableType',
                'frontend_input'         => 'ruleInputType',
                'is_filterable'          => 'ruleFacetSelectType',
                'is_user_defined'        => 'ruleRevert',
                'sellvana_multilanguage' => 'ruleZero'
            ),
            'skip' => array('skipField')
        ));
    }

    /**
     * @inheritdoc
     */
    protected function _export()
    {
        /** @var Mage_Eav_Model_Config $attributes */
        $attributes = Mage::getModel($this->_magentoModelName);
        $attributes = $attributes
            ->getEntityType(Mage_Catalog_Model_Product::ENTITY)
            ->getAttributeCollection();

        if (!sizeof($attributes)) {
            return null;
        }

        $attributeCodes = array();

        /** @var Mage_Catalog_Model_Resource_Eav_Attribute $model */
        foreach ($attributes as $model) {

            if ($this->skip($model)) {
                continue;
            }

            $attributeCode = $model->getData('attribute_code');
            $attributeCodes[$attributeCode]['id'] = $model->getId();

            $modelData = $this->_prepareData($model);

            $attributeCodes[$attributeCode]['type'] = $model->getData('backend_type');

            $this->writeToFile($modelData);
        }

        $this->_storage->setCatalogFieldsData('attribute_codes', $attributeCodes, 'field');

        return $this;
    }

    /**
     * @param Mage_Catalog_Model_Resource_Eav_Attribute $model
     * @return bool
     */
    public function skipField(Mage_Catalog_Model_Resource_Eav_Attribute $model)
    {
        $return = !$this->ruleInputType($model, 'frontend_input');

        if (!$return && $model->getData('is_user_defined') == 0) {
            $return = true;
        }

        return $return;
    }

    /**
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleType(Varien_Object $model, $attribute)
    {
        $model->setData($attribute, 'product');
        return true;
    }

    /**
     * @param Mage_Eav_Model_Entity_Attribute $model
     * @param $attribute
     * @return bool
     */
    public function ruleTableType(Mage_Eav_Model_Entity_Attribute $model, $attribute)
    {
        $inputType = $model->getData('frontend_input');
        $tableType = $model->getData($attribute);

        switch($inputType){
            case 'select':
            case 'multiselect':
                $tableType = 'options';
                break;
            case 'boolean':
                $tableType = 'tinyint';
                break;
        }

        $model->setData($attribute, $tableType);
        return true;
    }

    /**
     * TODO: add to skip rule;
     * @param Mage_Eav_Model_Entity_Attribute $model
     * @param $attribute
     * @return bool
     */
    public function ruleInputType(Mage_Eav_Model_Entity_Attribute $model, $attribute)
    {
        $inputType = $model->getData($attribute);

        $isWysiwyg = $model->getData('is_wysiwyg_enabled');
        if ($isWysiwyg && $inputType != 'weee') {
            $model->setData($attribute, 'wysiwyg');
            return true;
        }

        switch ($inputType) {
            case 'text':
            case 'date':
            case 'price':
            case 'media_image':
                $inputType = 'text';
                break;
            case 'textarea':
                $inputType = 'textarea';
                break;
            case 'boolean':
                $inputType = 'boolean';
                break;
            case 'multiselect':
                $inputType = 'multiselect';
                break;
            case 'select':
                $inputType = 'select';
                break;
            case 'weee':
            default:
                return false;
                break;
        }

        $model->setData($attribute, $inputType);

        return true;
    }

    /**
     * @param Mage_Core_Model_Abstract $model
     * @param $attribute
     * @return bool
     */
    public function ruleFacetSelectType(Mage_Core_Model_Abstract $model, $attribute)
    {
        $model->setData($attribute, $model->getData($attribute) ? 'Inclusive': 'No');

        return true;
    }
}