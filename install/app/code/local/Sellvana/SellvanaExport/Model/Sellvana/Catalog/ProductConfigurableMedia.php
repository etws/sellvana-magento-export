<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductConfigurableMedia
 */
class Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductConfigurableMedia
    extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    const MEDIA_TYPE_IMG    = 'I'
        , MEDIA_TYPE_ATTACH = 'A';

    protected $_sellvanaModelName = 'Sellvana_Catalog_Model_ProductMedia';
    protected $_magentoModelName  = 'catalog/product';
    protected $_modelGroups       = array(
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_PRODUCT
    );
    protected $_uniqueKey        = array('product_id', 'file_id');

    protected $_lastAutoIncrementId = 1;

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap = array(
        'id'              => 'virtual_auto_increment',
        'product_id'      => 'PK',
        'media_type'      => 'sellvana_media_type',
        'file_id'         => 'sellvana_file_id',
        //'file_path'       => '',
        //'remote_url'      => 'sellvana_remote_url',
        //'data_serialized' => 'sellvana_data',
        //'label'           => '',
        'position'        => 'sellvana_position',
        'is_thumb'        => 'sellvana_is_thumb',
        'is_default'      => 'sellvana_is_default',
        //'is_rollover'     => '',
        'in_gallery'      => 'sellvana_in_gallery'
    );

    protected $_mediaAttributes = array();

    protected $_mediaVariants = array();

    /** @var int */
    protected $_counter           = 0;


    /**
     * @inheritdoc
     */
    public function _construct()
    {
        parent::_construct();

        /**
         * @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductConfigurable::_export,
         * @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductSimple::_export
         */
        $this->setData('processed_products', $this->_storage->getProductData('processed', 'products'));
    }

    /**
     * @inheritdoc
     */
    protected function _prepareCollection(Varien_Data_Collection_Db $collection)
    {
        parent::_prepareCollection($collection);

        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection->addAttributeToFilter('type_id', array('in' => array('configurable')));
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge_recursive(parent::rules(), array(
            'validate' => array(
                /** @see ruleVirtualAutoIncrement(), ruleString() */
                'virtual_auto_increment' => array('ruleVirtualAutoIncrement', 'ruleString'),
                /** @see ruleSellvanaMediaType() */
                'sellvana_media_type'    => 'ruleSellvanaMediaType',
                /** @see ruleString() */
                'sellvana_file_id'      => 'ruleString',
                /** @see ruleString() */
                'sellvana_position'      => 'ruleString',
                /** @see ruleInt(), ruleString() */
                'sellvana_is_thumb'      => array('ruleInt', 'ruleString'),
                /** @see ruleInt(), ruleString() */
                'sellvana_is_default'    => array('ruleInt', 'ruleString'),
                /** @see ruleInt(), ruleString() */
                'sellvana_in_gallery'    => array('ruleInt', 'ruleString')
            ),
            /** @see skipNotConfigurable() */
            'skip' => 'skipNotConfigurable'
        ));
    }

    protected function _export()
    {
        $return = parent::_export();

        //$this->_storage->setMediaLibraryData('variants', $this->_mediaVariants, 'media');
    }


    /**
     * @inheritdoc
     */
    protected function _prepareData(Varien_Object $model, $map = null)
    {
        //$this->log->start(get_class($this) . '-' . $model->getId());
        if (null === $map) {
            $map = $this->_defaultFieldsMap;
        }


        /** @var Sellvana_SellvanaExport_Helper_Array $arrayHelper */
        $arrayHelper = Mage::helper('sellvana_sellvanaexport/array');

        $this->_loadMediaGallery($model);

        $mediaGallery = $model->getData('media_gallery');
        $images = $mediaGallery['images'];

        $files = $arrayHelper->map($images, 'value_id', 'file');

        $return = array();
        $maxPosition = 1;
        $addedImageFileNames = array();
        foreach ($images as $image) {
            $tmpModel = clone $model;

            $isDefault = (array_search($tmpModel->getData('image'), $files) == $image['value_id']);
            $isThumbnail = (array_search($tmpModel->getData('thumbnail'), $files) == $image['value_id']);

            $valueId = $image['value_id'];
            $addedImageFileNames[] = $image['file'];
            $storageId = $this->_storage->getMediaLibraryData($image['file'], 'productImageFile');
            if (null !== $storageId) {
                $valueId = $storageId;
            }

            $tmpModel
                ->setData('sellvana_file_id', $valueId)
                ->setData('sellvana_position', $image['position'])
                ->setData('sellvana_is_default', $isDefault)
                ->setData('sellvana_is_thumb', $isThumbnail);

            $maxPosition = $image['position'] > $maxPosition ? $image['position'] : $maxPosition;

            $result = array();
            foreach ($map as $field => $attribute) {
                $result[$field] = $this->getValidatedData($attribute, $tmpModel);
            }
            $this->_storage->setMediaLibraryData(
                $model->getId() . '/' . $valueId,
                $tmpModel->getData('virtual_auto_increment'),
                'productMediaId'
            );
            $return[] = json_encode(array_values($result));
        }

        /** @var Mage_Catalog_Model_Product_Type_Configurable $typeInstance */
        $typeInstance = $model->getTypeInstance();
        $children = $typeInstance->getUsedProductCollection();

        $childImages = array();
        foreach ($children as $child) {
            /** @var Mage_Catalog_Model_Product $child */
            $this->_loadMediaGallery($child);
            $mediaGallery = $child->getData('media_gallery');
            $childImages = array_merge($childImages, $mediaGallery['images']);
        }

        foreach ($childImages as $image) {
            if (in_array($image['file'], $addedImageFileNames)) {
                continue;
            }

            $tmpModel = new Varien_Object();

            //$valueId = $image['value_id'];
            $valueId = null;
            $storageId = $this->_storage->getMediaLibraryData($image['file'], 'productImageFile');
            if (null !== $storageId) {
                $valueId = $storageId;
            }

            $tmpModel
                ->setData('PK', $model->getId())
                ->setData('sellvana_file_id', $valueId)
                ->setData('sellvana_position', $maxPosition++)
                ->setData('sellvana_is_default', 0)
                ->setData('sellvana_is_thumb', 0);

            $result = array();
            foreach ($map as $field => $attribute) {
                $result[$field] = $this->getValidatedData($attribute, $tmpModel);
            }

            $this->_mediaVariants[$valueId] = $tmpModel->getData('virtual_auto_increment');
            $this->_storage->setMediaLibraryData(
                $model->getId() . '/' . $valueId,
                $tmpModel->getData('virtual_auto_increment'),
                'productMediaId'
            );
            $return[] = json_encode(array_values($result));
            $addedImageFileNames[] = $image['file'];
        }

        if (($this->_counter++ % 50) == 0) {
            gc_collect_cycles();
        }

        //$this->log->end(get_class($this) . '-' . $model->getId());
        return $return;
    }

    /**
     * @param Varien_Data_Collection_Db $collection
     * @return void
     */
    protected function _addAttributesToCollection(Varien_Data_Collection_Db $collection)
    {
        parent::_addAttributesToCollection($collection);

        if ($collection instanceof Mage_Eav_Model_Entity_Collection_Abstract) {
            $collection->addAttributeToSelect(array('thumbnail', 'image'));
        }
    }

    /**
     * Check if is necessary to skip product
     * If product doesn't have to be skipped, add it id to @see $_processed
     *
     * @param Mage_Catalog_Model_Product $model
     * @return bool
     */
    public function skipNotConfigurable(Mage_Catalog_Model_Product $model)
    {
        $processedProducts = $this->getData('processed_products');

        $return = ($model->getTypeId() !== 'configurable');
        if (!$return) {
            $processedProducts[$model->getId()] = 'configurable';
        }

        return $return;
    }

    /**
     * @param Varien_Object $model
     * @param $attribute
     * @return bool
     */
    public function ruleSellvanaMediaType(Varien_Object $model, $attribute)
    {
        $model->setData($attribute, self::MEDIA_TYPE_IMG);
        return true;
    }

    /**
     * @param Varien_Object $model
     * @throws Mage_Core_Exception
     */
    protected function _loadMediaGallery(Varien_Object $model)
    {
        $attributeSetId = $model->getAttributeSetId();
        if (!isset($this->_mediaAttributes[$attributeSetId])) {
            $this->_mediaAttributes[$attributeSetId] = $model->getTypeInstance(true)->getSetAttributes($model);
        }

        $key = 'media_gallery';
        /** @var Mage_Catalog_Model_Resource_Eav_Attribute $mediaGallery */
        $mediaGallery = $this->_mediaAttributes[$attributeSetId][$key];
        $backend = $mediaGallery->getBackend();
        $backend->afterLoad($model);
    }
}