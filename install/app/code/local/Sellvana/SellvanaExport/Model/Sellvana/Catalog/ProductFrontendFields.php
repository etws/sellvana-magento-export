<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Vadims Bucinskis <v.buchinsky@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductFrontendFields
 */
class Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductFrontendFields
    extends Sellvana_SellvanaExport_Model_Sellvana_Abstract
{
    protected $_sellvanaModelName = 'Sellvana_Catalog_Model_Product';
    protected $_magentoModelName  = 'catalog/product';
    protected $_modelGroups       = array(
        Sellvana_SellvanaExport_Model_System_Config_Source_ModelGroups::MODEL_GROUP_PRODUCT
    );
    protected $_uniqueKey        = 'product_sku';

    /** @var array SellvanaField => MagentoField|MagentoAttribute */
    protected $_defaultFieldsMap = array(
        'id'           => 'PK',
        'product_sku'  => 'sku',//check to overwrites
        '_custom_data' => 'sellvana_custom_data',
    );

    protected $_serializedFieldsMap = array(
        'id'                 => 'option_id',
        'name'               => 'default_title',
        'label'              => 'default_title',
        'input_type'         => 'type',
        'required'           => 'is_require',
        'options'            => 'values',
        'position'           => 'sort_order',
        'group'              => '',
        'qty_min'            => '',
        'qty_max'            => '',
        'options_serialized' => 'values_data'
    );

    protected $_typeMap = array(
        Mage_Catalog_Model_Product_Option::OPTION_TYPE_FIELD        => 'text',
        Mage_Catalog_Model_Product_Option::OPTION_TYPE_AREA         => 'textarea',
        //Mage_Catalog_Model_Product_Option::OPTION_TYPE_FILE         => '',
        Mage_Catalog_Model_Product_Option::OPTION_TYPE_DROP_DOWN    => 'select',
        Mage_Catalog_Model_Product_Option::OPTION_TYPE_RADIO        => 'select',
        Mage_Catalog_Model_Product_Option::OPTION_TYPE_CHECKBOX     => 'checkbox',
        Mage_Catalog_Model_Product_Option::OPTION_TYPE_MULTIPLE     => 'text',
        Mage_Catalog_Model_Product_Option::OPTION_TYPE_DATE         => 'text',
        Mage_Catalog_Model_Product_Option::OPTION_TYPE_DATE_TIME    => 'text',
        Mage_Catalog_Model_Product_Option::OPTION_TYPE_TIME         => 'text'
    );

    protected $_skipTypes = array(
        Mage_Catalog_Model_Product_Option::OPTION_TYPE_FILE
    );

    /**
     * @inheritdoc
     */
    public function _construct()
    {
        parent::_construct();

        /**
         * @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductConfigurable::_export,
         * @see Sellvana_SellvanaExport_Model_Sellvana_Catalog_ProductSimple::_export
         * */
        $this->setData('processed_products', $this->_storage->getProductData('processed', 'products'));
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        //ruleUnique is to slow for big data.
        return array_merge_recursive(parent::rules(), array(
            'validate' => array(
                /** @see ruleCustomData() */
                'sellvana_custom_data' => 'ruleCustomData'
            ),
            'skip' => array(
                /** @see skipProduct() */
                'skipProduct'
            )
        ));
    }

    /**
     * @inheritdoc
     */
    protected function _prepareCollection(Varien_Data_Collection_Db $collection)
    {
        parent::_prepareCollection($collection);

        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection->addAttributeToFilter('type_id', array('in' => array('simple', 'configurable')));
        $collection->addOptionsToResult();
    }


    protected function ruleCustomData(Mage_Catalog_Model_Product $model, $attribute)
    {
        $options = $model->getOptions();

        $autoIncrement = 1;
        /** @var Mage_Catalog_Model_Product_Option $item */
        foreach ($options as $item) {
            $optionData = $item->getData();

            // skip unknown option types
            if (!isset($this->_typeMap[$optionData['type']])) {
                continue;
            }

            $data = array(
                'id'            => $autoIncrement++,
                'name'          => $optionData['default_title'],
                'label'         => $optionData['default_title'],
                'input_type'    => $this->_typeMap[$optionData['type']],
                'require'       => $optionData['is_require'],
                'position'      => $optionData['sort_order'],
                //'group'         => '',
                //'qty_min'       => '',
                //'qty_max'       => '',
            );

            $values = $item->getValues();
            if (!empty($values)) {
                $optionValues = array();
                /** @var Mage_Catalog_Model_Product_Option_Value $value */
                foreach ($values as $value) {
                    $valueData = $value->getData();
                    $valueId = preg_replace('/[^A-Za-z0-9_-]/', '-', $valueData['default_title']);
                    $optionValues[$valueId] = array (
                        'sku' => $valueData['default_title'],
                        'position' => $valueData['sort_order'],
                        'prices' => array(
                            (object)array(
                                'id' => '1',
                                'product_id' => '1',
                                'customer_group_id' => '*',
                                'site_id' => '*',
                                'currency_code' => '*',
                                'price_type' => 'base',
                                'amount' => '20',
                                'operation' => '=$',
                                'qty' => '1'
                            )
                        )
                    );
                }

                $data['options'] = implode(',', array_keys($optionValues));
                $data['options_serialized'] = json_encode((object) $optionValues);
            }


            $tmpModel = new Varien_Object();
            $tmpModel->setData(array(
                'sku' => $model->getData('sku'),

            ));
        }

        $model->setData($attribute, (object)$data);

        return true;
    }

    /**
     * Check if is necessary to skip product
     *
     * @param Mage_Catalog_Model_Product $model
     * @return bool
     */
    public function skipProduct(Mage_Catalog_Model_Product $model)
    {
        $processedProducts = $this->getData('processed_products');
        $productId = $model->getId();

        if (!array_key_exists($productId, $processedProducts)) {
            return true;
        }

        return (empty($model->getOptions()));
    }
}