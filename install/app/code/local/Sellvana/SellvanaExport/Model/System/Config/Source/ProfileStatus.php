<?php
class Sellvana_SellvanaExport_Model_System_Config_Source_ProfileStatus
{
    public function toOptionArray()
    {
        return array(
            Sellvana_SellvanaExport_Model_Profile::STATUS_NEW => 'New',
            Sellvana_SellvanaExport_Model_Profile::STATUS_SCHEDULED => 'Scheduled',
            Sellvana_SellvanaExport_Model_Profile::STATUS_INPROGRESS => 'In progress',
            Sellvana_SellvanaExport_Model_Profile::STATUS_FINISHED => 'Finished',
        );
    }
}