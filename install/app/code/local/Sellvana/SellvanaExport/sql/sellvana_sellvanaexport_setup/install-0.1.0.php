<?php
/* @var $this Mage_Core_Model_Resource_Setup */

$this->startSetup();

$table = $this->getConnection()
    ->newTable($this->getTable('sellvana_sellvanaexport/profile'))
    ->addColumn('profile_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Profile ID')
    ->addColumn('file', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable' => false
    ))
    ->addColumn('entities', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(
        'nullable' => false
    ))
    ->addColumn('last_success', Varien_Db_Ddl_Table::TYPE_DATETIME);

$this->getConnection()->createTable($table);

$this->endSetup();