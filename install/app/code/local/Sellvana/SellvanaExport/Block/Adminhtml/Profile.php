<?php
/**
 * Copyright 2015 Sellvana Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @package Sellvana
 * @link https://www.sellvana.com/
 * @author Alexey Yerofeyev <a.yerofeyev@etwebsolutions.com>
 * @copyright (c) 2010-2014 Boris Gurvich
 * @license http://www.apache.org/licenses/LICENSE-2.0.html
 */

/**
 * Class Sellvana_SellvanaExport_Block_Adminhtml_Profile
 */
class Sellvana_SellvanaExport_Block_Adminhtml_Profile extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        $this->_blockGroup      = 'sellvana_sellvanaexport';
        $this->_controller      = 'adminhtml_profile';
        $this->_headerText      = $this->__('Export Profiles');
        $this->_addButtonLabel  = $this->__('Add New Profile');
        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    public function getCreateUrl()
    {
        return $this->getUrl('*/*/new');
    }

}

